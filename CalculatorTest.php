<?php
use PHPUnit\Framework\TestCase;
require 'Calculator.php';
/**
 * 
 */
class CalculatorTest extends TestCase
{
	public function setUp() :void
	{
	    $this->calculator = new Calculator();
	 }
	public function testEstablishConnection()
    {
        $actual = $this -> calculator -> esatblishConnection(DBHOST, DBUSER, DBPSWD, DBNAME);

        $this->assertInstanceOf(mysqli::class, $actual);
    }

	public function testEvaluateExpression()
	{
		$actual = $this -> calculator -> evaluateExpression("16+7-8*6+2/2");
		$expected = -24; //integer
		$this->assertEquals($expected, $actual);
	}

	public function testWriteResultIntoDB()
	{
		$conn = new mysqli(DBHOST, DBUSER, DBPSWD, DBNAME);
		$myString = "-5.99999";

		$this->assertNotFalse($this -> calculator -> writeResultIntoDB($myString, $conn));
	}

	public function testRetrieveLast5ResultsFromDB()
	{
		$conn = new mysqli(DBHOST, DBUSER, DBPSWD, DBNAME);
		$actual = $this -> calculator -> retrieveLast5ResultsFromDB($conn);

		$this->assertInstanceOf(mysqli_stmt::class, $actual); //if mysqli_stmt object
	}

	public function testMysqliResponseObjToArray()
	{
		$conn = new mysqli(DBHOST, DBUSER, DBPSWD, DBNAME);
		$db_record = $this -> calculator -> retrieveLast5ResultsFromDB($conn);
		$array_repres_db_record = $this -> calculator -> mysqliResponseObjToArray($db_record);

		//if this assertion passes it will assert that $array_repres_db_record is array
		$this->assertIsString($array_repres_db_record[0]); //confirm if it is array and if the first element of array is string
	}
}

?>