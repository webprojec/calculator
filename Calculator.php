<?php
include "config.php"; //database credentials

class Calculator
{

  public function evaluateExpression($expression)
  {
    $math_string = "return ".$expression.";";
    return eval($math_string);
  }

  public function esatblishConnection($dbhost, $dbuser, $dbpswd, $dbname) 
  {
    $connection = mysqli_connect($dbhost, $dbuser, $dbpswd, $dbname);
    if (mysqli_connect_errno()) {
          throw new \RuntimeException(mysqli_connect_error());
    }
    return $connection;
  }

  public function handleTableInDB($connection)
  {
    $check_if_table_exists = "SHOW tables like 'calc_results'";
    if($check_table_response = mysqli_query($connection, $check_if_table_exists)) {
      
      if($check_table_response -> num_rows === 1) {
        return; //skip if table exists
      } else {
        $create_table_if_not_exists = $connection -> prepare("CREATE table calc_results (id int not null auto_increment, result varchar(255) not null, primary key(id))");
        if($create_table_if_not_exists !== false) {
          $create_table_if_not_exists -> execute();
          if($create_table_if_not_exists === false) {
            throw new \RuntimeException(mysqli_connect_error()); //could not execute the query
          }
        } else {
          throw new \RuntimeException("Error while preparing query");
        }
      }
      return;
    } 
      
    throw new \RuntimeException(mysqli_connect_error()); //connection problems
  }

  public function writeResultIntoDB($stringValue, $connection)
  {
    $query_insert = $connection -> prepare("INSERT into calc_results (result) values ('" . $stringValue . "')");
    if($query_insert !== false) {
      $query_insert -> execute();
      if($query_insert === false) {
        throw new \RuntimeException(mysqli_connect_error());
      }
    } else {
      throw new \RuntimeException("Error while preparing query");
    }

    return true;
    
  }

  public function retrieveLast5ResultsFromDB($connection)
  {
    $select_last5 = $connection -> prepare("SELECT result from calc_results order by id desc limit 5");
    if($select_last5 !== false) {
      $select_last5 -> execute();
      if($select_last5 === false) {
        throw new \RuntimeException(mysqli_connect_error());
      }
    } else {
      throw new \RuntimeException("Error while preparing query");
    }
    return $select_last5;
  }

  public function mysqliResponseObjToArray($mysqliResponseObj)
  {
    $res_col_array = [];

    foreach ($mysqliResponseObj -> get_result() as $row) {
      $res_col_array[] = $row['result'];
    }

    return array_reverse($res_col_array);
  }
}



function flow()
{
  $data = $_POST["toEvaluate"];
  //print_r($data);

  $calc = new Calculator();
  $result = $calc -> evaluateExpression($data);

  if (!extension_loaded('mysqli')) {
    $db_record = false;
  } else {
    $conn = $calc -> esatblishConnection(DBHOST, DBUSER, DBPSWD, DBNAME);

    $calc -> handleTableInDB($conn); //check if db has the table 'calc_results', if not then create one
    
    $calc -> writeResultIntoDB(strval($result), $conn);

    $last5results = $calc -> retrieveLast5ResultsFromDB($conn);

    $db_record = $calc -> mysqliResponseObjToArray($last5results);

    $conn -> close();
  }
  

  $response = [
    'response' => $result,
    'db_record' => $db_record
  ];

  echo json_encode($response);
}

flow();
?>
